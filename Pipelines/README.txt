The pipelines included in the folder "Pipelines" can be run in RStudio. 
The R version used to run these pipelines is 4.0.4

The libraries used in the markdowns along with their versions are:
DT: 0.19
AnnotationDbi: 1.52.0
GO.db: 3.12.1
biomaRt: 2.46.3
dendextend: 1.15.2
circlize: 0.4.13
compositions: 2.0-2
VennDiagram: 1.6.20
gridExtra: 2.3
ggplot2: 3.3.5
plotly: 4.10.0
Rpdb: 2.3
bio3d: 2.4-2
readxl: 1.3.1
ggpubr: 0.4.0
------------------------------------------------------------------------------------------
First: run the markdown titled pipeline1 in order to get the list of neuronal membranal genes.
Note: The first markdown will create a directory called "Pipeline" where all the work is based. 
Make sure to include all the files in the folder "Supplementary Material" in that directory.

Second: run the markdown titled pipeline2 in order to filter the neuronal membranal genes per neuronal compartment.

Third: run the markdown titled pipeline3 in order to analyze the protein families of the neuronal membranal genes.

Fourth: run the markdown titled pipeline4 in order to analyze the PDB structures of the neuronal membranal mice genes.

Fifth: run the markdown titled pipeline5 in order to calculate and estimate surface areas for the neuronal membranal mice genes.

Sixth: run the markdown titled pipeline6 in order to put everything together in the model to estimate the concentration of mice ion channels in the neuron after obtaining relative expressions of neuronal membranal mice genes.
------------------------------------------------------------------------------------------
Note: All the pipelines have been run. The files output upon running can be found in the folder titled "FilesGeneratedUponRunningMarkdowns".
The knitting outputs were stored in the same folder in the form of HTML pages having the same names of the markdowns (pipeline1.html, pipeline2.html, pipeline3.html, etc.).